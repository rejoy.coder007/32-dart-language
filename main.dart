import 'dart:isolate';
import 'dart:io';
void foo(var message){


  for(var i=0;i<10;i++) {
    print('execution from foo ... the message is :${message} :: ${i}');
    sleep(const Duration(seconds:1));
  }

}
void main(){
  Isolate.spawn(foo,'A');
  Isolate.spawn(foo,'B');

  //foo("A");
  //foo("B");
  //Isolate.spawn(foo,'Greetings!!');
 // Isolate.spawn(foo,'Welcome!!');
  print('execution from main1');
  print('execution from main2');
  print('execution from main3');
  print('-----------------------------');

  //This is while loop for infinite loop
  while(true);
}